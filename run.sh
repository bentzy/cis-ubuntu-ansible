#!/bin/bash

#return zero if all commands exit successfully
set -e -o pipefail

args=''

###get level
#cislevel=$1
#if [ "$cislevel" != "level1" ]
#then
#   echo "Only CIS Level1 is currently supported. Exiting."
#   exit -1
#else
#   to_skip+="level2"
#fi

to_skip+="level2"

#check distribution
distribution=$(lsb_release -sc)

if [ "$distribution" = "bionic" ] || [ "$distribution" = "xenial" ]
then
   to_skip+=",not-for-$distribution"
else
   echo "Only bionic & xenial are currently supported. Exiting."
   exit -1
fi


#TODO: check this: 2.2.4 && 3.7 level depends on workstation/server


install_dependencies () {

sudo apt-get update
sudo apt-get -y install python-pip
sudo -H pip install --upgrade pip
sudo -H pip install --upgrade setuptools
sudo apt-get -y install git python-dev
sudo -H pip install ansible markupsafe fstab

local_co=""
if [ -z "$IP" ]; then
  IP="127.0.0.1"
  local_co="-c local"
  #sudo apt-get install -y aptitude
fi

}

install_playbook () {
mkdir -p ansible/roles-ubuntu/roles
cd ansible/roles-ubuntu
if [ ! -e "roles/cis" ]; then
  git clone https://gitlab.com/bentzy/cis-ubuntu-ansible.git roles/cis --branch master
fi

if [ ! -e "playbook.yml" ]; then
  cat >> playbook.yml << 'EOF'
---
- hosts: all
  roles:
    - cis
EOF
fi

}

run_playbook () {
ansible-playbook -b -u $USER $local_co -i "$IP," playbook.yml --skip-tags $to_skip
}

purge () {
sudo apt-get -y purge python-pip git python-dev python-apt
sudo apt-get -y  autoremove
pip freeze | xargs sudo -H pip uninstall -y
#rm -rf /tmp/*
}


cd /tmp
install_dependencies
install_playbook
run_playbook | tee /tmp/cis.log
purge

#DEBUG:
#use
#pushd /tmp/ansible/roles-ubuntu
# ansible-playbook -b -u $USER -c local -i "127.0.0.1," playbook.yml --tags section01.01 --skip-tags level2
#purge
