# CIS for Ubuntu 16.04 and 18.04

Based on https://github.com/veritone/cis-ubuntu-ansible
and Uber's mountopts https://github.com/Uberspace/ansible-mountopts, 
BUT completely reworked and trimmed to adapt it to our needs.
## Supported CIS Benchmarks:
##         --  CIS_Ubuntu_Linux_16.04_LTS_Benchmark_v1.1.0.pdf
##         --  CIS_Ubuntu_Linux_18.04_LTS_Benchmark_v1.0.0.pdf

# 1. Supports solely CIS benchmark for Ubuntu Server 16 & 18, Level1 
# 2. Intended to run on cloud based virtual instances. Not applicable sections were excluded
# 3. Edit defaults/main.yml to fulfill your policies
# 4. Reboot required to apply all changes
 

## Usage

### One liner installation & execution

### The following will automatically install Ansible, download and run the playbook on your local system:

```
$ \curl -sSL https://gitlab.com/bentzy/cis-ubuntu-ansible/raw/master/run.sh > /tmp/cis.sh && bash /tmp/cis.sh
```
